genie - a tool for tagging arbitrary file paths with arbitarty tags

# Installation

1. Clone the project
2. Modify `dbpath` variable on line #76
3. Build and install with `cargo install`

# Usage

The first time that you use genie, you must run `genie init` to set up
the db as required.

Use `genie --help` to see usage instructions

# License

genie is copyright 2017 zach wick <zach@zachwick.com> and is licensed
under the GNU GPLv3 or later. You can find a copy of the GNU GPLv3
included in the project as the file named `License`.
